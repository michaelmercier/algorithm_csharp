﻿//source: www.w3resource.com/csharp-exercises/basic-algo/index.php
using System;
using System.Linq;

namespace MikeTest
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            //Generate Algorith1 test data.
            Console.WriteLine("Algorithm1");
            Console.WriteLine(Algorithm1(1, 2));
            Console.WriteLine(Algorithm1(3, 2));
            Console.WriteLine(Algorithm1(2, 2));
            //Generate Algorith2 test data.
            Console.WriteLine("Algorithm2");
            Console.WriteLine(Algorithm2(53));
            Console.WriteLine(Algorithm2(30));
            Console.WriteLine(Algorithm2(51));
            //Generate Algorith3 test data.
            Console.WriteLine("Algorithm3");
            Console.WriteLine(Algorithm3(30, 0));
            Console.WriteLine(Algorithm3(25, 5));
            Console.WriteLine(Algorithm3(20, 30));
            Console.WriteLine(Algorithm3(20, 25));
            //Generate Algorithm4 test data.
            Console.WriteLine("Algorithm4");
            Console.WriteLine(Algorithm4(103));
            Console.WriteLine(Algorithm4(90));
            Console.WriteLine(Algorithm4(89));
            //Generate Algorithm5 test data.
            Console.WriteLine("Algorithm5");
            Console.WriteLine(Algorithm5("if else"));
            Console.WriteLine(Algorithm5("else"));
            //Generate Algorithm6 test data.
            Console.WriteLine("Algorithm6");
            Console.WriteLine(Algorithm6("Python", 1));
            Console.WriteLine(Algorithm6("Python", 0));
            Console.WriteLine(Algorithm6("Python", 4));
            //Generate Algorithm7 test data.
            Console.WriteLine("Algorithm7");
            Console.WriteLine(Algorithm7("abcd"));
            Console.WriteLine(Algorithm7("a"));
            Console.WriteLine(Algorithm7("xy"));
            //Generate Algorithm8 test data.
            Console.WriteLine("Algorithm8");
            Console.WriteLine(Algorithm8("C Sharp"));
            Console.WriteLine(Algorithm8("JS"));
            Console.WriteLine(Algorithm8("a"));
            //Generate Algorithm9 test data.
            Console.WriteLine("Algorithm9");
            Console.WriteLine(Algorithm9("Red"));
            Console.WriteLine(Algorithm9("Green"));
            Console.WriteLine(Algorithm9("1"));
            //Generate Algorithm10 test data.
            Console.WriteLine("Algorithm10");
            Console.WriteLine(Algorithm10(3));
            Console.WriteLine(Algorithm10(14));
            Console.WriteLine(Algorithm10(12));
            Console.WriteLine(Algorithm10(37));
            //Generate Algorithm11 test data.
            Console.WriteLine("Algorithm11");
            Console.WriteLine(Algorithm11("Python"));
            Console.WriteLine(Algorithm11("JS"));
            Console.WriteLine(Algorithm11("Code"));
            //Generate Algorithm12 test data.
            Console.WriteLine("Algorithm12");
            Console.WriteLine(Algorithm12("C# Sharp"));
            Console.WriteLine(Algorithm12("C#"));
            Console.WriteLine(Algorithm12("C++"));
            //Generate Algorithm13 test data.
            Console.WriteLine("Algorithm13");
            Console.WriteLine(Algorithm13(120, -1));
            Console.WriteLine(Algorithm13(-1, 120));
            Console.WriteLine(Algorithm13(2, 120));
            //Generate Algorithm14 test data.
            Console.WriteLine("Algorithm14");
            Console.WriteLine(Algorithm14(100, 199));
            Console.WriteLine(Algorithm14(250, 300));
            Console.WriteLine(Algorithm14(105, 190));
            //Generate Algorithm15 test data.
            Console.WriteLine("Algorithm15");
            Console.WriteLine(Algorithm15(11, 20, 12));
            Console.WriteLine(Algorithm15(30, 30, 17));
            Console.WriteLine(Algorithm15(25, 35, 50));
            Console.WriteLine(Algorithm15(15, 12, 8));
            //Skipped #16 - #99
            //Generate Algorithm100 test data.
            Console.WriteLine("Algorithm100");
            Console.WriteLine(Algorithm100(new[] { 12, 20 }));
            Console.WriteLine(Algorithm100(new[] { 20, 20 }));
            Console.WriteLine(Algorithm100(new[] { 10, 10 }));
            Console.WriteLine(Algorithm100(new[] { 10 }));
            //Generate Algorithm101 test data.
            Console.WriteLine("Algorithm101");
            var answer101 = Algorithm101(new[] { 1, 5, 7 });
            Console.Write("New Array : ");
            foreach (int i in answer101)
            {
                Console.Write(i.ToString() + ' ');
            }
            Console.Write("\n");
            //Generate Algorithm102 test data.
            Console.WriteLine("Algorithm102");
            var answer102 = Algorithm102(new[] { 10, 20, -30 }, new[] { 10, 20, 30 });
            Console.Write("Winner is : ");
            foreach (int i in answer102)
            {
                Console.Write(i.ToString() + ' ');
            }
            Console.Write("\n");
            //Generate Algorithm103 test data.
            Console.WriteLine("Algorithm103");
            var answer103 = Algorithm103(new[] { 1, 5, 7, 9, 11, 13 });
            foreach (int i in answer103)
            {
                Console.Write(i.ToString() + ' ');
            }
            Console.Write("\n");
            //Generate Algorithm104 test data.
            Console.WriteLine("Algorithm104");
            Console.Write("New array: ");
            var answer104 = Algorithm104(new[] { 10, 20, 30 }, new[] { 40, 50, 60 });
            foreach (int i in answer104)
            {
                Console.Write(i.ToString() + ' ');
            }
            Console.Write("\n");
            //Generate Algorithm105 test data.
            Console.WriteLine("Algorithm105");
            Console.Write("After swapping first and last elements: ");
            var answer105 = Algorithm105(new[] { 1, 5, 7, 9, 11, 13 });
            foreach (int i in answer105)
            {
                Console.Write(i.ToString() + ' ');
            }
            Console.Write("\n");
            //Generate Algorithm106 test data.
            Console.WriteLine("Algorithm106");
            Console.Write("New array: ");
            var answer106 = Algorithm106(new[] { 1, 5, 7, 9, 11, 13 });
            foreach (int i in answer106)
            {
                Console.Write(i.ToString() + ' ');
            }
            Console.Write("\n");
            //Generate Algorithm107 test data.
            Console.WriteLine("Algorithm107");
            Console.WriteLine(Algorithm107(new[] { 1 }));
            Console.WriteLine(Algorithm107(new[] { 1, 2, 9 }));
            Console.WriteLine(Algorithm107(new[] { 1, 2, 9, 3, 3 }));
            Console.WriteLine(Algorithm107(new[] { 1, 2, 3, 4, 5, 6, 7 }));
            Console.WriteLine(Algorithm107(new[] { 1, 2, 2, 3, 7, 8, 9, 10, 6, 5, 4 }));
            //Generate Algorithm108 test data.
            Console.WriteLine("Algorithm108");
            var answer108 = Algorithm108(new[] { 1, 5, 7, 9, 11, 13 });
            foreach (int i in answer108)
            {
                Console.Write(i.ToString() + ' ');
            }
            Console.Write("\n");
            //Generate Algorithm109 test data.
            Console.WriteLine("Algorithm109");
            Console.WriteLine(Algorithm109(new[] { 1, 5, 7, 9, 10, 12 }));
            //Generate Algorithm110 test data.
            Console.WriteLine("Algorithm110");
            Console.WriteLine(Algorithm110(new[] { 1, 5, 7, 9, 10, 12 }));
            //Generate Algorithm111 test data.
            Console.WriteLine("Algorithm111");
            Console.WriteLine(Algorithm111(new[] { 1, 5, 7, 9, 10, 17 }));
            //Generate Algorithm112 test data.
            Console.WriteLine("Algorithm112");
            Console.WriteLine(Algorithm112(new[] { 5, 6, 1, 5, 6, 9, 10, 17, 5, 6 }));
            Console.WriteLine(Algorithm112(new[] { 5, 6, 1, 5, 6, 9, 10, 17 }));
            Console.WriteLine(Algorithm112(new[] { 1, 5, 6, 9, 10, 17, 5, 6 }));
            Console.WriteLine(Algorithm112(new[] { 1, 5, 9, 10, 17, 5, 6 }));
            Console.WriteLine(Algorithm112(new[] { 1, 5, 9, 10, 17, 5 }));
            //Generate Algorithm113 test data.
            Console.WriteLine("Algorithm113");
            Console.WriteLine(Algorithm113(new[] { 1, 5, 6, 9, 10, 17 }));
            Console.WriteLine(Algorithm113(new[] { 1, 5, 5, 9, 10, 17 }));
            Console.WriteLine(Algorithm113(new[] { 1, 5, 5, 9, 10, 17, 5, 5 }));
            //Generate Algorithm114 test data.
            Console.WriteLine("Algorithm114");
            Console.WriteLine(Algorithm114(new[] { 1, 5, 6, 9, 10, 17 }));
            Console.WriteLine(Algorithm114(new[] { 1, 4, 7, 9, 5, 17 }));
            Console.WriteLine(Algorithm114(new[] { 1, 1, 2, 9, 10, 17 }));
            //Generate Algorithm115 test data.
            Console.WriteLine("Algorithm115");
            Console.WriteLine(Algorithm115(new[] { 1, 5, 6, 9, 10, 17 }));
            Console.WriteLine(Algorithm115(new[] { 1, 5, 5, 5, 10, 17 }));
            Console.WriteLine(Algorithm115(new[] { 1, 1, 5, 5, 5, 5 }));
            //Generate Algorithm116 test data.
            Console.WriteLine("Algorithm116");
            Console.WriteLine(Algorithm116(new[] { 1, 5, 6, 9, 3, 3 }));
            Console.WriteLine(Algorithm116(new[] { 1, 5, 5, 5, 10, 17 }));
            Console.WriteLine(Algorithm116(new[] { 1, 3, 3, 5, 5, 5 }));
            //Generate Algorithm117 test data.
            Console.WriteLine("Algorithm117");
            Console.WriteLine(Algorithm117(new[] { 5, 5, 5, 5, 5 }));
            Console.WriteLine(Algorithm117(new[] { 3, 3, 3, 3 }));
            Console.WriteLine(Algorithm117(new[] { 3, 3, 3, 5, 5, 5 }));
            Console.WriteLine(Algorithm117(new[] { 1, 6, 8, 10 }));
            //Generate Algorithm118 test data.
            Console.WriteLine("Algorithm118");
            Console.WriteLine(Algorithm118(new[] { 5, 5, 5, 5, 5 }));
            Console.WriteLine(Algorithm118(new[] { 3, 3, 3, 3 }));
            Console.WriteLine(Algorithm118(new[] { 3, 3, 3, 5, 5, 5 }));
            Console.WriteLine(Algorithm118(new[] { 1, 6, 8, 10 }));
            //Generate Algorithm119 test data.
            Console.WriteLine("Algorithm119");
            Console.WriteLine(Algorithm119(new[] { 5, 5, 5, 5, 5 }));
            Console.WriteLine(Algorithm119(new[] { 1, 2, 3, 4 }));
            Console.WriteLine(Algorithm119(new[] { 3, 3, 5, 5, 5, 5 }));
            Console.WriteLine(Algorithm119(new[] { 1, 5, 5, 7, 8, 10 }));
            //Generate Algorithm120 test data.
            Console.WriteLine("Algorithm120");
            Console.WriteLine(Algorithm120(new[] { 5, 5, 1, 5, 5 }));
            Console.WriteLine(Algorithm120(new[] { 1, 2, 3, 4 }));
            Console.WriteLine(Algorithm120(new[] { 3, 3, 5, 5, 5, 5 }));
            Console.WriteLine(Algorithm120(new[] { 1, 5, 5, 7, 8, 10 }));
            //Generate Algorithm121 test data.
            Console.WriteLine("Algorithm121");
            Console.WriteLine(Algorithm121(new[] { 3, 5, 1, 3, 7 }));
            Console.WriteLine(Algorithm121(new[] { 1, 2, 3, 4 }));
            Console.WriteLine(Algorithm121(new[] { 3, 3, 5, 5, 5, 5 }));
            Console.WriteLine(Algorithm121(new[] { 2, 5, 5, 7, 8, 10 }));
            //Generate Algorithm122 test data.
            Console.WriteLine("Algorithm122");
            Console.WriteLine(Algorithm122(new[] { 3, 5, 1, 3, 7 }));
            Console.WriteLine(Algorithm122(new[] { 1, 2, 3, 4 }));
            Console.WriteLine(Algorithm122(new[] { 3, 3, 5, 5, 5, 5 }));
            Console.WriteLine(Algorithm122(new[] { 2, 4, 5, 6 }));
            //Generate Algorithm123 test data.
            Console.WriteLine("Algorithm123");
            Console.WriteLine(Algorithm123(new[] { 3, 5, 1, 5, 3, 5, 7, 5, 1, 5 }));
            Console.WriteLine(Algorithm123(new[] { 3, 5, 5, 5, 5, 5, 5 }));
            Console.WriteLine(Algorithm123(new[] { 3, 5, 2, 5, 4, 5, 7, 5, 8, 5 }));
            Console.WriteLine(Algorithm123(new[] { 2, 4, 5, 5, 5, 5 }));
            //Generate Algorithm124 test data.
            Console.WriteLine("Algorithm124");
            Console.WriteLine(Algorithm124(new[] { 3, 5, 5, 3, 7 }));
            Console.WriteLine(Algorithm124(new[] { 3, 5, 5, 4, 1, 5, 7 }));
            Console.WriteLine(Algorithm124(new[] { 3, 5, 5, 5, 5, 5 }));
            Console.WriteLine(Algorithm124(new[] { 2, 4, 5, 5, 6, 7, 5 }));
            //Generate Algorithm125 test data.
            Console.WriteLine("Algorithm125");
            Console.WriteLine(Algorithm125(new[] { 3, 7, 5, 5, 3, 7 }, 2));
            Console.WriteLine(Algorithm125(new[] { 3, 7, 5, 5, 3, 7 }, 3));
            Console.WriteLine(Algorithm125(new[] { 3, 7, 5, 5, 3, 7, 5 }, 3));

            Console.ReadLine();
        }
        private static int Algorithm1(int value1, int value2)
        {
            return value1 == value2 ? (value1 + value2) * 3 : value1 + value2;
        }
        private static int Algorithm2(int n)
        {
            const int x = 51;

            return n > x ? (n - x) * 3 : x - n;
        }
        private static bool Algorithm3(int x, int y)
        {
            const int goal = 30;
            return x == goal || y == goal || (x + y == goal) ? true : false;
        }
        private static bool Algorithm4(int x)
        {
            return Math.Abs(x - 100) <= 10 || Math.Abs(x - 200) <= 10 ? true : false;
        }
        private static string Algorithm5(string s)
        {
            return s.Length > 2 && s.Substring(0, 2).Equals("if") ? s : "if " + s;
        }
        private static string Algorithm6(string s, int x)
        {
            return s.Length >= x && x >= 0 ? s.Remove(x, 1) : "condition is false";
        }
        private static string Algorithm7(string s)
        {
            return s.Length > 1 ? s.Substring(s.Length - 1) + s.Substring(1, s.Length - 2) + s.Substring(0, 1) : s;
        }
        private static string Algorithm8(string s)
        {
            return s.Length > 1 ? string.Concat(Enumerable.Repeat(s.Substring(0, 2), 4)) : s;
        }
        private static string Algorithm9(string s)
        {
            return s.Length > 0 ? s.Substring(s.Length - 1) + s.Substring(0, s.Length) + s.Substring(s.Length - 1) : "Error";
        }
        private static bool Algorithm10(int x)
        {
            return x % 3 == 0 || x % 7 == 0;
        }
        private static string Algorithm11(string s)
        {
            return s.Length > 2 ? s.Substring(0, 3) + s + s.Substring(0, 3) : s + s + s;
        }
        private static bool Algorithm12(string s)
        {
            return (s.Length < 3 && s.Equals("C#")) || (s.StartsWith("C#") && s[2] == ' ');
        }
        private static bool Algorithm13(int x, int y)
        {
            const int TEMP1 = 0;
            const int TEMP2 = 100;
            return (x < TEMP1 || y < TEMP1) && (x > TEMP2 || y > TEMP2) ? true : false;
        }
        private static bool Algorithm14(int x, int y)
        {
            const int MIN = 100;
            const int MAX = 200;
            return (x >= MIN && x <= MAX) && (y >= MIN && y <= MAX);
        }
        private static bool Algorithm15(int x, int y, int z)
        {
            return (x >= 20 && x <= 50) || (y >= 20 && y <= 50) || (z >= 20 && z <= 50);
        }
        private static bool Algorithm100(int[] item)
        {
            const int MIN = 10;
            const int MAX = 20;
            return item.Length == 2 && ((item[0] == MIN && item[1] == MIN) || (item[0] == MAX && item[1] == MAX));
        }
        private static int[] Algorithm101(int[] numbers)
        {
            for (int i = 0; i < numbers.Length - 1; i++)
            {
                if (numbers[i].Equals(5) && numbers[i + 1].Equals(7))
                    numbers[i + 1] = 1;
            }
            return numbers;
        }
        private static int[] Algorithm102(int[] sum1, int[] sum2)
        {
            return sum1[0] + sum1[1] + sum1[2] >= sum2[0] + sum2[1] + sum2[2] ? sum1 : sum2;
        }
        private static int[] Algorithm103(int[] num)
        {
            return new int[] { num[num.Length / 2 - 1], num[num.Length / 2] };
        }
        private static int[] Algorithm104(int[] array1, int[] array2)
        {
            return new int[] { array1[0], array1[1], array1[2], array2[0], array2[1], array2[2] };
        }
        private static int[] Algorithm105(int[] numbers)
        {
            int first = numbers[0];
            numbers[0] = numbers[numbers.Length - 1];
            numbers[numbers.Length - 1] = first;
            return numbers;
        }
        private static int[] Algorithm106(int[] numbers)
        {
            return new int[] { numbers[numbers.Length / 2 - 1], numbers[numbers.Length / 2], numbers[numbers.Length / 2 + 1] };
        }
        private static int Algorithm107(int[] numbers)
        {
            int first = numbers[0];
            int mid = numbers[numbers.Length / 2];
            int last = numbers[numbers.Length - 1];
            int result = first;

            if (result < mid)
                result = mid;

            if (result < last)
                result = last;

            return result;
        }
        private static int[] Algorithm108(int[] numbers)
        {
            return numbers.Length > 1 ? new int[] { numbers[0], numbers[1] } : numbers;
        }
        private static int Algorithm109(int[] numbers)
        {
            int count = 0;
            foreach (int i in numbers)
            {
                if (i % 2 == 0)
                    count++;
            }
            return count;
        }
        private static int Algorithm110(int[] numbers)
        {
            int max = 0;
            int min = 0;
            if (numbers.Length > 0)
                min = max = numbers[0];

            foreach (int i in numbers)
            {
                min = Math.Min(min, i);
                max = Math.Max(max, i);
            }
            return max - min;
        }
        private static int Algorithm111(int[] numbers)
        {
            int total = 0;
            foreach (int i in numbers)
            {
                if (i != 17)
                    total += i;
            }
            return total;
        }
        private static int Algorithm112(int[] numbers)
        {
            int total = 0;
            for (int i = 0; i < numbers.Length; i++)
            {
                if (i < numbers.Length - 1)
                {
                    if (numbers[i] == 5 && numbers[i + 1] == 6)
                        i++;
                    else
                        total += numbers[i];
                }
                else
                    total += numbers[i];
            }
            return total;
        }
        private static bool Algorithm113(int[] numbers)
        {
            for (int i = 0; i < numbers.Length - 1; i++)
            {
                if (numbers[i] == 5 && numbers[i + 1] == 5)
                    return true;
            }
            return false;
        }
        private static bool Algorithm114(int[] numbers)
        {
            bool bFive = false;
            bool bSeven = false;
            foreach (int i in numbers)
            {
                if (i == 5)
                    bFive = true;
                if (i == 7)
                    bSeven = true;
                if (bFive && bSeven)
                    return bFive && bSeven;
            }
            return false;
        }
        private static bool Algorithm115(int[] numbers)
        {
            int sum = 0;
            foreach (int i in numbers)
            {
                if (i == 5)
                    sum += i;
            }
            return sum == 15;
        }
        private static bool Algorithm116(int[] numbers)
        {
            int countOf3 = 0, countOf5 = 0;
            foreach (int i in numbers)
            {
                if (i == 3)
                    countOf3++;
                if (i == 5)
                    countOf5++;
            }
            return countOf3 > countOf5;
        }
        private static bool Algorithm117(int[] numbers)
        {
            foreach (int i in numbers)
            {
                if (i == 3 || i == 5)
                    return true;
            }
            return false;
        }
        private static bool Algorithm118(int[] numbers)
        {
            bool bThree = false;
            bool bFive = false;

            foreach (int i in numbers)
            {
                if (i == 3)
                    bThree = true;
                if (i == 5)
                    bFive = true;
            }
            return bThree && bFive ? false : true;
        }
        private static bool Algorithm119(int[] numbers)
        {
            for (int i = 0; i < numbers.Length - 1; i++)
            {
                if ((numbers[i] == 3 && numbers[i + 1] == 3) || (numbers[i] == 5 && numbers[i + 1] == 5))
                    return true;
            }
            return false;
        }
        private static bool Algorithm120(int[] numbers)
        {
            for (int i = 0; i < numbers.Length - 1; i++)
            {
                if (numbers[i] == 5 && numbers[i + 1] == 5)
                    return true;
                if (i + 2 < numbers.Length && numbers[i] == 5 && numbers[i + 2] == 5)
                    return true;
            }
            return false;
        }
        private static bool Algorithm121(int[] numbers)
        {
            bool bThree = false;
            foreach (int i in numbers)
            {
                if (i == 3)
                    bThree = true;
                if (bThree && i == 5)
                    return true;
            }
            return false;
        }
        private static bool Algorithm122(int[] numbers)
        {
            int tot_odd = 0, tot_even = 0;

            foreach (int i in numbers)
            {
                if (tot_odd < 2 && tot_even < 2)
                {
                    if (i % 2 == 0)
                    {
                        tot_even++;
                        tot_odd = 0;
                    }
                    else
                    {
                        tot_odd++;
                        tot_even = 0;
                    }
                }
            }
            return tot_odd == 2 || tot_even == 2;
        }
        private static bool Algorithm123(int[] numbers)
        {
            int countFive = 0;
            bool isFree = false;

            foreach (int i in numbers)
            {
                if (i == 5 && !isFree)
                {
                    isFree = true;
                    countFive++;
                }
                else
                {
                    isFree = false;
                }
            }
            return countFive == 5;
        }
        private static bool Algorithm124(int[] numbers)
        {
            bool answer = false;

            for (int i = 0; i < numbers.Length; i++)
            {
                if (numbers[i] == 5)
                {
                    if ((i < numbers.Length - 1 && numbers[i + 1] == 5) || (i > 0 && numbers[i - 1] == 5))
                        answer = true;
                    else
                        answer = false;
                }
            }
            return answer;
        }
        private static bool Algorithm125(int[] numbers, int x)
        {
            for (int i = 0; i < x; i++)
            {
                if (numbers[i] != numbers[numbers.Length - x + i])
                {
                    return false;
                }
            }
            return true;
        }
    }
}
